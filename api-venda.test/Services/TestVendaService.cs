using System.Globalization;
using ApiVenda.Application.DTO;
using ApiVenda.Core.Entities;
using ApiVenda.Core.Enums;
using ApiVenda.Core.Services;

namespace ApiVenda.Tests.Service;

[TestClass]
public class TestVendaService
{
    [TestMethod]
    public void GetAll_DeveRetornarTodasVendas()
    {
        var testVendas = GetTestVendas();
        var service = new VendaService(testVendas);

        var result = VendaService.GetAll();
        Assert.IsNotNull(result);
        Assert.AreEqual(testVendas.Count, result.Count);
    }

    [TestMethod]
    public void Get_DeveRetornarVendaEspecifica()
    {
        var testVendas = GetTestVendas();
        var service = new VendaService(testVendas);

        var result = VendaService.Get(125);
        Assert.IsNotNull(result);
        Assert.AreEqual(testVendas[3].Id, result.Id);
        Assert.AreEqual(testVendas[3].Total, result.Total);
    }

    [TestMethod]
    public void Get_NaoDeveEncontrarDeterminadaVenda()
    {
        var testVendas = GetTestVendas();
        var service = new VendaService(testVendas);

        var result = VendaService.Get(4);
        Assert.IsNull(result);
    }
    
    [TestMethod]
    public void Add_DeveIncluirUmaNovaVenda()
    {
        var testNovoIdVenda = 1;
        var testVendedorId = 5;
        var testItensVenda = TestItemVendaService.GetTestItensVendaRequest();
        var testVenda = new VendaRequest { IdVendedor = testVendedorId, ItensVenda = testItensVenda };

        var result = VendaService.Add(testVenda);
        Assert.IsNotNull(result);
        Assert.AreEqual(result.Id, testNovoIdVenda);
    }

    [TestMethod]
    public void Update_DevePermitirAtualizarStatusDaVendaPagamentoAprovadoParaEnviadoTransportadora()
    {
        var testVendas = GetTestVendas();
        var service = new VendaService(testVendas);
        var testVendaPagamentoAprovado = testVendas.Where(x => x.Status.Equals("Pagamento Aprovado")).SingleOrDefault();

        var testNovoStatus = StatusVenda.EnviadoTransportadora;
        var resultUpdate = VendaService.Update(testVendaPagamentoAprovado, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsTrue(resultUpdate);
    }

    [TestMethod]
    public void Update_DevePermitirAtualizarStatusDaVendaAguardandoPagamentoParaCancelada()
    {
        var testVendas = GetTestVendas();
        var service = new VendaService(testVendas);
        var testVendaStatusAguardandoPagamento = testVendas.Where(x => x.Status.Equals("Aguardando Pagamento")).SingleOrDefault();

        var testNovoStatus = StatusVenda.Cancelada;
        var resultUpdate = VendaService.Update(testVendaStatusAguardandoPagamento, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsTrue(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaAguardandoPagamentoParaEnviadoTransportadora()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusAguardandoPagamento = testVendas.Where(x => x.Status.Equals("Aguardando Pagamento")).SingleOrDefault();

        var testNovoStatus = StatusVenda.EnviadoTransportadora;
        var resultUpdate = VendaService.Update(testVendaStatusAguardandoPagamento, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }
    
    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaAguardandoPagamentoParaEntregue()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusAguardandoPagamento = testVendas.Where(x => x.Status.Equals("Aguardando Pagamento")).SingleOrDefault();

        var testNovoStatus = StatusVenda.Entregue;
        var resultUpdate = VendaService.Update(testVendaStatusAguardandoPagamento, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaPagamentoAprovadoParaAguardandoPagamento()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusPagamentoAprovado = testVendas.Where(x => x.Status.Equals("Pagamento Aprovado")).SingleOrDefault();

        var testNovoStatus = StatusVenda.AguardandoPagamento;
        var resultUpdate = VendaService.Update(testVendaStatusPagamentoAprovado, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaPagamentoAprovadoParaEntregue()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusPagamentoAprovado = testVendas.Where(x => x.Status.Equals("Pagamento Aprovado")).SingleOrDefault();

        var testNovoStatus = StatusVenda.Entregue;
        var resultUpdate = VendaService.Update(testVendaStatusPagamentoAprovado, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEnviadoTransportadoraParaAguardandoPagamento()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEnviadoTransportadora = testVendas.Where(x => x.Status.Equals("Enviado para Transportadora")).SingleOrDefault();

        var testNovoStatus = StatusVenda.AguardandoPagamento;
        var resultUpdate = VendaService.Update(testVendaStatusEnviadoTransportadora, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEnviadoTransportadoraParaPagamentoAprovado()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEnviadoTransportadora = testVendas.Where(x => x.Status.Equals("Enviado para Transportadora")).SingleOrDefault();

        var testNovoStatus = StatusVenda.PagamentoAprovado;
        var resultUpdate = VendaService.Update(testVendaStatusEnviadoTransportadora, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEnviadoTransportadoraParaCancelada()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEnviadoTransportadora = testVendas.Where(x => x.Status.Equals("Enviado para Transportadora")).SingleOrDefault();

        var testNovoStatus = StatusVenda.Cancelada;
        var resultUpdate = VendaService.Update(testVendaStatusEnviadoTransportadora, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEntregueParaAguardandoPagamento()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEntregue = testVendas.Where(x => x.Status.Equals("Entregue")).SingleOrDefault();

        var testNovoStatus = StatusVenda.AguardandoPagamento;
        var resultUpdate = VendaService.Update(testVendaStatusEntregue, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEntregueParaPagamentoAprovado()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEntregue = testVendas.Where(x => x.Status.Equals("Entregue")).SingleOrDefault();

        var testNovoStatus = StatusVenda.PagamentoAprovado;
        var resultUpdate = VendaService.Update(testVendaStatusEntregue, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEntregueParaEnviadoTransportadora()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEntregue = testVendas.Where(x => x.Status.Equals("Entregue")).SingleOrDefault();

        var testNovoStatus = StatusVenda.EnviadoTransportadora;
        var resultUpdate = VendaService.Update(testVendaStatusEntregue, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaEntregueParaCancelada()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusEntregue = testVendas.Where(x => x.Status.Equals("Entregue")).SingleOrDefault();

        var testNovoStatus = StatusVenda.Cancelada;
        var resultUpdate = VendaService.Update(testVendaStatusEntregue, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaCanceladaParaAguardandoPagamento()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusCancelada = testVendas.Where(x => x.Status.Equals("Cancelada")).SingleOrDefault();

        var testNovoStatus = StatusVenda.AguardandoPagamento;
        var resultUpdate = VendaService.Update(testVendaStatusCancelada, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaCanceladaParaPagamentoAprovado()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusCancelada = testVendas.Where(x => x.Status.Equals("Cancelada")).SingleOrDefault();

        var testNovoStatus = StatusVenda.PagamentoAprovado;
        var resultUpdate = VendaService.Update(testVendaStatusCancelada, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate); 
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaCanceladaParaEnviadoTransportadora()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusCancelada = testVendas.Where(x => x.Status.Equals("Cancelada")).SingleOrDefault();

        var testNovoStatus = StatusVenda.EnviadoTransportadora;
        var resultUpdate = VendaService.Update(testVendaStatusCancelada, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate); 
    }

    [TestMethod]
    public void Update_NaoDevePermitirAtualizarStatusVendaCanceladaParaEntregue()
    {
        var testVendas = GetTestVendas();
        var testVendaStatusCancelada = testVendas.Where(x => x.Status.Equals("Cancelada")).SingleOrDefault();

        var testNovoStatus = StatusVenda.Entregue;
        var resultUpdate = VendaService.Update(testVendaStatusCancelada, testNovoStatus);
        Assert.IsNotNull(resultUpdate);
        Assert.IsFalse(resultUpdate);   
    }

    private List<Venda> GetTestVendas()
    {
        var testProdutos = TestProdutoService.GetTestProdutos();
        var testVendedores = TestVendedorService.GetTestVendedores();
        var testItensVenda = TestItemVendaService.GetTestItensVenda();

        var testVenda = new List<Venda>()
        {
            new()
            {
                Id = 35,
                Vendedor = testVendedores[2],
                ItensVenda = testItensVenda,
                Total = VendaService.GetTotalVenda(testItensVenda),
                Status = "Aguardando Pagamento",
                DataVenda = DateTime.Now.AddDays(-4).ToString(CultureInfo.CreateSpecificCulture("pt-BR")),
                DataUtilmaAtualizacaoVenda = null
            },
            new() {
                Id = 8,
                Vendedor = testVendedores[0],
                ItensVenda = testItensVenda,
                Total = VendaService.GetTotalVenda(testItensVenda),
                Status = "Pagamento Aprovado",
                DataVenda = DateTime.Now.AddDays(-7).ToString(CultureInfo.CreateSpecificCulture("pt-BR")),
                DataUtilmaAtualizacaoVenda = DateTime.Now.AddDays(-1).ToString(CultureInfo.CreateSpecificCulture("pt-BR"))
            },
            new() {
                Id = 17,
                Vendedor = testVendedores[1],
                ItensVenda = testItensVenda,
                Total = VendaService.GetTotalVenda(testItensVenda),
                Status = "Enviado para Transportadora",
                DataVenda = DateTime.Now.AddDays(-2).ToString(CultureInfo.CreateSpecificCulture("pt-BR")),
                DataUtilmaAtualizacaoVenda = DateTime.Now.ToString(CultureInfo.CreateSpecificCulture("pt-BR"))
            },
            new() {
                Id = 125,
                Vendedor = testVendedores[2],
                ItensVenda = testItensVenda,
                Total = VendaService.GetTotalVenda(testItensVenda),
                Status = "Entregue",
                DataVenda = DateTime.Now.AddDays(-20).ToString(CultureInfo.CreateSpecificCulture("pt-BR")),
                DataUtilmaAtualizacaoVenda = DateTime.Now.AddDays(-8).ToString(CultureInfo.CreateSpecificCulture("pt-BR"))
            },
            new() {
                Id = 1,
                Vendedor = testVendedores[1],
                ItensVenda = testItensVenda,
                Total = VendaService.GetTotalVenda(testItensVenda),
                Status = "Cancelada",
                DataVenda = DateTime.Now.AddDays(-14).ToString(CultureInfo.CreateSpecificCulture("pt-BR")),
                DataUtilmaAtualizacaoVenda = DateTime.Now.AddDays(-9).ToString(CultureInfo.CreateSpecificCulture("pt-BR"))
            }
        };

        return testVenda;
    }
}