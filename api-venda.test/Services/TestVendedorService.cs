using ApiVenda.Core.Entities;
using ApiVenda.Core.Services;

namespace ApiVenda.Tests.Service;

[TestClass]
public class TestVendedorService
{
    [TestMethod]
    public void GetAll_DeveRetornarTodosVendedoresCadastrados()
    {
        var testVendedores = GetTestVendedores();
        VendedorService vendedorService = new(testVendedores);

        var result = VendedorService.GetAll();
        Assert.IsNotNull(result);
        Assert.AreEqual(result.Count, testVendedores.Count);
    }

    [TestMethod]
    public void Get_DeveRetornarVendedorEspecifico()
    {
        var testVendedores = GetTestVendedores();
        VendedorService vendedorService = new(testVendedores);

        var result = VendedorService.Get(9);
        Assert.IsNotNull(result);
        Assert.AreEqual(result.Id, testVendedores[1].Id);
        Assert.AreEqual(result.Nome, testVendedores[1].Nome);
    }

    [TestMethod]
    public void Get_NaoDeveEncontrarDeterminadoVendedor()
    {
        var testVendedores = GetTestVendedores();
        VendedorService vendedorService = new(testVendedores);

        var result = VendedorService.Get(289);
        Assert.IsNull(result);
    }

    public static List<Vendedor> GetTestEmptyVendedores() => [];

    public static List<Vendedor>? GetTestNoVendedores() => null;

    public static List<Vendedor> GetTestVendedores()
    {
        var testVendedor = new List<Vendedor>()
        {
            new()
            {
                Id = 4,
                CPF = "489.473.980-12",
                Nome = "Vindr the Mist-walker",
                Telefone = "(32) 97655-2846",
                Email = "m.walker@pottencial.com"
            },
            new()
            {
                Id = 9,
                CPF = "948.353.790-80",
                Nome = "Randi the Mead-brewer",
                Telefone = "(64) 98955-2726",
                Email = "r.brewer@pottencial.com"
            },
            new()
            {
                Id = 18,
                CPF = "841.363.950-60",
                Nome = "Ziabu Boawa",
                Telefone = "(91) 97235-2149",
                Email = "z.boawa@pottencial.com"
            }
        };

        return testVendedor;
    }
} 