using ApiVenda.Application.DTO;
using ApiVenda.Core.Entities;
using ApiVenda.Core.Services;

namespace ApiVenda.Tests.Service;

[TestClass]
public class TestItemVendaService 
{
    [TestMethod]
    public void Get_DeveRetornarDetalhesItensVenda()
    {
        var testItensVendaRequest = GetTestItensVendaRequest();
        var result = ItemVendaService.Get(testItensVendaRequest);

        Assert.AreEqual(testItensVendaRequest.Count, result.Count);
    }

    [TestMethod]
    public void Get_DeveRetornarDetalhesItensVendaVazioPorNaoExistirItemNaVenda()
    {
        var testItensVendaRequest = GetTestEmptyItensVendaRequest();
        var testItensVenda = GetTestEmptyItensVenda();

        var result = ItemVendaService.Get(testItensVendaRequest);

        Assert.IsNotNull(result);
        Assert.IsInstanceOfType(result, typeof(List<ItemVenda>));
        Assert.AreEqual(result.Count, testItensVenda.Count);
    }

    [TestMethod]
    public void Get_DeveRetornarDetalhesItensVendaVazioPorItemVendaSerNulo()
    {
        var testItensVendaRequest = GetTestNoItensVendaRequest();
        var testItensVenda = GetTestEmptyItensVenda();
        
        var result = ItemVendaService.Get(testItensVendaRequest);

        Assert.IsNotNull(result);
        Assert.IsInstanceOfType(result, typeof(List<ItemVenda>));
        Assert.AreEqual(result.Count, testItensVenda.Count);
    }

    public static List<ItemVendaRequest> GetTestEmptyItensVendaRequest() => [];

    public static List<ItemVendaRequest>? GetTestNoItensVendaRequest() => null;

    public static List<ItemVendaRequest> GetTestItensVendaRequest()
    {
        var testItensVenda = new List<ItemVendaRequest>()
        {
            new() { IdProduto = 1, Quantidade = 2 },
            new() { IdProduto = 2, Quantidade = 1 },
            new() { IdProduto = 3, Quantidade = 5 },
            new() { IdProduto = 85, Quantidade = 1 }
        };

        return testItensVenda;
    }

    public static List<ItemVenda> GetTestEmptyItensVenda() => [];

    public static List<ItemVenda>? GetTestNoItensVenda() => null;

    public static List<ItemVenda> GetTestItensVenda()
    {
        var testProdutos = TestProdutoService.GetTestProdutos();

        var testItensVenda = new List<ItemVenda>()
        {
            new() { Produto = testProdutos[1], Quantidade = 2, Total = testProdutos[1].Valor * 2 },
            new() { Produto = testProdutos[3], Quantidade = 5, Total = testProdutos[3].Valor * 5 },
            new() { Produto = testProdutos[0], Quantidade = 1, Total = testProdutos[1].Valor }
        };

        return testItensVenda;
    }
}