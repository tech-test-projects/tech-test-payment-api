using ApiVenda.Core.Entities;
using ApiVenda.Core.Services;

namespace ApiVenda.Tests.Service;

[TestClass]
public class TestProdutoService
{
    [TestMethod]
    public void GetAll_DeveRetornarTodosProdutosCadastrados()
    {
        var testProdutos = GetTestProdutos();
        ProdutoService produtoService = new(testProdutos);

        var result = ProdutoService.GetAll();
        Assert.IsNotNull(result);
        Assert.AreEqual(testProdutos.Count, result.Count);
    }

    [TestMethod]
    public void Get_DeveRetornarProdutoEspecifico()
    {
        var testProdutos = GetTestProdutos();
        ProdutoService produtoService = new(testProdutos);

        var result = ProdutoService.Get(7);
        Assert.IsNotNull(result);
        Assert.AreEqual(testProdutos[2].Nome, result.Nome);
    }

    [TestMethod]
    public void Get_NaoDeveEncontrarDeterminadoProduto()
    {
        var testProdutos = GetTestProdutos();
        ProdutoService produtoService = new(testProdutos);

        var result = ProdutoService.Get(123);
        Assert.IsNull(result);
    }

    public static List<Produto> GetTestEmptyProdutos() => [];

    public static List<Produto>? GetTestNoProdutos() => null;

    public static List<Produto> GetTestProdutos()
    {
        var testProduto = new List<Produto>()
        {
            new() { Id = 1, Nome = "Asynchronous Programming in Rust", Descricao = "Learn asynchronous programming by building working examples of futures, green threads, and runtimes", Valor = 178.99 },
            new() { Id = 2, Nome = "Observability with Grafana", Descricao = "Monitor, control, and visualize your Kubernetes and cloud platforms using the LGTM stack", Valor = 222.99 },
            new() { Id = 7, Nome = "Modern DevOps Practices", Descricao = "Implement, secure, and manage applications on the public cloud by leveraging cutting-edge tools", Valor = 768.10 },
            new() { Id = 32, Nome = "The Software Developer's Guide to Linux", Descricao = "A practical, no-nonsense guide to using the Linux command line and utilities as a software developer", Valor = 143.19 },            
            new() { Id = 67, Nome = "C# 11 and .NET 7 - Modern Cross-Platform Development Fundamentals", Descricao = "Start building websites and services with ASP.NET Core 7, Blazor, and EF Core 7", Valor = 170.34 }
        };
        
        return testProduto;
    }
}