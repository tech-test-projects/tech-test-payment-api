namespace ApiVenda.Core.Entities;

public class Venda
{
    public int Id { get; set; }

    public Vendedor? Vendedor { get; set; }

    public List<ItemVenda>? ItensVenda { get; set; }

    public string Total { get; set; } = "0";

    public string? Status { get; set; }

    public string? DataVenda { get; set; }

    public string? DataUtilmaAtualizacaoVenda { get; set; }
}