using ApiVenda.Application.DTO;
using ApiVenda.Core.Entities;

namespace ApiVenda.Core.Services;

public static class ItemVendaService 
{
    static ItemVendaService()
    {
    }

    public static List<ItemVenda> Get(List<ItemVendaRequest>? itensVenda)
    {
        var response = new List<ItemVenda>();

        if (itensVenda is null)
            return response;

        foreach (var item in itensVenda)
        {
            response.Add(GetItem(item.IdProduto, item.Quantidade));
        }

        return response;
    }

    private static ItemVenda GetItem(int idProduto, int quantidade)
    {
        var produto = ProdutoService.Get(idProduto);

        if (produto is null)
            return new();

        return new() { 
            Produto = produto, 
            Quantidade = quantidade, 
            Total = TotalItensVenda(produto.Valor, quantidade) 
        };
    } 

    private static double TotalItensVenda(double valor, int quantidade)
    {
        return valor * quantidade;
    }
}