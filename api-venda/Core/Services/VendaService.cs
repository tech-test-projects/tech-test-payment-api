using ApiVenda.Application.DTO;
using ApiVenda.Application.Utils;
using ApiVenda.Core.Entities;
using ApiVenda.Core.Enums;

namespace ApiVenda.Core.Services;

public class VendaService
{
    static int novoId = 0;

    static List<Venda> Vendas { get; set; }
    
    static VendaService()
    {
        Vendas = [];
    }

    public VendaService(List<Venda> vendas)
    {
        Vendas = vendas;
    }

    public static int GetNewVendaId()
    {
        return ++novoId;
    }

    public static List<Venda> GetAll() => Vendas;

    public static Venda? Get(int id) => Vendas.FirstOrDefault(v => v.Id == id);
    
    public static Venda Add(VendaRequest request)
    {
        var itensVenda = ItemVendaService.Get(request.ItensVenda);
        var venda = new Venda()
        {
            Id = GetNewVendaId(),
            Vendedor = VendedorService.Get(request.IdVendedor),
            ItensVenda = itensVenda,
            Status = Util.GetDescriptionEnum(StatusVenda.AguardandoPagamento),
            Total = GetTotalVenda(itensVenda),
            DataVenda = Util.GerarDataHoraExataFormatoPtBr()
        };

        Vendas.Add(venda);
        return venda;
    }

    public static bool Update(Venda venda, StatusVenda novoStatusVenda)
    {
        if (!PermiteAlterarStatusVenda(venda.Status, novoStatusVenda))
            return false;

        var index = Vendas.FindIndex(v => v.Id == venda.Id);

        if (index == -1)
            return false;

        venda.Status = Util.GetDescriptionEnum(novoStatusVenda);
        venda.DataUtilmaAtualizacaoVenda = Util.GerarDataHoraExataFormatoPtBr();
        Vendas[index] = venda;

        return true;
    }

    public static string GetTotalVenda(List<ItemVenda> itensVenda)
    {
        double totalVenda = 0;

        foreach (var item in itensVenda)
        {
            totalVenda += item.Total;
        }

        return Util.FormatarMoeda(totalVenda);
    }

    static bool PermiteAlterarStatusVenda(string statusVenda, StatusVenda novoStatusVenda)
    {
        switch (statusVenda)
        {
            case "Entregue":
            case "Cancelado":
                return false;
            case "Aguardando Pagamento":
                if (novoStatusVenda == StatusVenda.PagamentoAprovado || 
                    novoStatusVenda == StatusVenda.Cancelada)
                    return true;

                return false;
            case "Pagamento Aprovado":
                if (novoStatusVenda == StatusVenda.EnviadoTransportadora || 
                    novoStatusVenda == StatusVenda.Cancelada)
                    return true;

                return false;
            case "Enviado para Transportadora":
                return novoStatusVenda == StatusVenda.Entregue;
            default:
                return false;
        }
    }
}