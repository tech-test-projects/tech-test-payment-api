using ApiVenda.Core.Entities;

namespace ApiVenda.Core.Services;

public class VendedorService
{
    static List<Vendedor> Vendedores { get; set; }

    static VendedorService()
    {
        Vendedores =
        [
            new () { Id = 1, Nome = "Robert Lewandowski", CPF = "017.012.480-00", Email = "r.lewandowski@pottencial.com", Telefone = "(63) 97424-2606" },
            new () { Id = 2, Nome = "Ronaldinho Gaúcho", CPF = "886.512.770-83", Email = "r.gaucho@pottencial.com", Telefone = "(95) 97532-2462" },
            new () { Id = 3, Nome = "Andrea Pirlo", CPF = "647.588.620-36", Email = "a.pirlo@pottencial.com", Telefone = "(82) 97937-2236" },
            new () { Id = 4, Nome = "Andriy Shevchenko", CPF = "489.473.980-12", Email = "a.shevchenko@pottencial.com", Telefone = "(84) 97643-2831" },
            new () { Id = 5, Nome = "Zinédine Zidane", CPF = "917.611.240-30", Email = "z.zidane@pottencial.com", Telefone = "(63) 98965-5679" },
        ];
    }

    public VendedorService(List<Vendedor> vendedores)
    {
        Vendedores = vendedores;
    }

    public static List<Vendedor> GetAll() => Vendedores;

    public static Vendedor? Get(int id) => Vendedores.FirstOrDefault(v => v.Id == id); 
} 