using ApiVenda.Core.Entities;

namespace ApiVenda.Core.Services;

public class ProdutoService
{
    static List<Produto> Produtos { get; set; }

    static ProdutoService()
    {
        Produtos =
        [
            new () { Id = 1, Nome = "Notebook Acer AN515-58-54UH", Descricao = "ACER NITRO 5 • Processador Intel® Core™ i5-12450H da 12ª Geração série H com 8 núcleos • Tela 15,6” IPS de 144Hz com resolução Full HD", Valor = 4637.41 },
            new () { Id = 2, Nome = "Cadeira Gamer Profissional TGC12", Descricao = "Modelo: TGC12 - Cor: Preta", Valor = 1059.98 },
            new () { Id = 3, Nome = "Samsung Smartwatch Galaxy Watch6 LTE", Descricao = "O Galaxy Watch6 LTE é o smartwatch que oferece o mais completo conjunto de funções de monitoramento para o acompanhamento da Saúde e do Bem-Estar", Valor = 1890 },
            new () { Id = 4, Nome = "Clamper Energia 5 Tomadas", Descricao = "Um DPS Classe III, com capacidade de dreno de corrente de 13.500 amperes.", Valor = 59.99 },
            new () { Id = 5, Nome = "Jogo bits ponteiras 1/4 com 58 peças", Descricao = "1 Jogo de bits/ponteiras, composto por um estojo plástico com 58 peças", Valor = 101.96 },
            new () { Id = 6, Nome = "Mala American Tourister", Descricao = "Bagagem com Centro Rígido Expansível com Rodas Giratórias", Valor = 459 }
        ];
    }

    public ProdutoService(List<Produto>? produtos)
    {
        Produtos = produtos?? new List<Produto>();
    }

    public static List<Produto> GetAll() => Produtos;

    public static Produto? Get(int id) => Produtos.FirstOrDefault(p => p.Id == id);
}