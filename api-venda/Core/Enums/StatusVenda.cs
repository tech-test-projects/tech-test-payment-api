using System.ComponentModel;

namespace ApiVenda.Core.Enums;

public enum StatusVenda
{
    [Description("Aguardando Pagamento")]
    AguardandoPagamento = 1,

    [Description("Pagamento Aprovado")]
    PagamentoAprovado = 2,
    
    [Description("Enviado para Transportadora")]
    EnviadoTransportadora = 3,
    
    [Description("Entregue")]
    Entregue = 4,
    
    [Description("Cancelada")]
    Cancelada = 5
}