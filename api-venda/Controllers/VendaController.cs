using ApiVenda.Application.DTO;
using ApiVenda.Core.Entities;
using ApiVenda.Core.Enums;
using ApiVenda.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace ApiVenda.Controllers;

[ApiController]
[Route("[controller]")]
public class VendaController : ControllerBase
{
    /// <summary>
    /// Busca por todas as Vendas realizadas.
    /// </summary>
    [HttpGet]
    public ActionResult<List<Venda>> ObterVendas() => VendaService.GetAll();

    /// <summary>
    /// Permite efetuar uma busca de determinda venda, através de seu Identificador.
    /// </summary>
    /// <param name="id">Identificador da Venda a ser buscada.</param>
    [HttpGet("{id}")]
    public ActionResult<Venda> ObterVenda(int id)
    {
        var venda = VendaService.Get(id);

        if (venda is null)
            return NotFound();

        return venda;
    }

    /// <summary>
    /// Registra uma Nova Venda.
    /// </summary>
    /// <param name="request">Dados da Venda a ser cadastrada no sistema.</param>
    [HttpPost]
    public IActionResult RegistrarVenda(VendaRequest request)
    {
        if (request is null)
            return BadRequest();

        var vendedor = VendedorService.Get(request.IdVendedor);

        if (vendedor is null)
            return BadRequest();

        if (request.ItensVenda is null || 
            request.ItensVenda.Count == 0)
            return BadRequest();

        var venda = VendaService.Add(request);
        return CreatedAtAction(nameof(ObterVenda), new { id = venda.Id }, venda); 
    }

    /// <summary>
    /// Atualiza informação do Status da Venda no sistema.
    /// </summary>
    /// <param name="id">Identificador da Venda.</param>
    /// <param name="status">Novo Status que a Venda deve possuir.</param>
    [HttpPut("{id}")]
    public IActionResult AtualizarStatusVenda(int id, StatusVenda status)
    {
        var venda = VendaService.Get(id);

        if (venda is null)
            return NotFound();
        
        if (VendaService.Update(venda, status))
            return NoContent();

        return BadRequest();
    }
}