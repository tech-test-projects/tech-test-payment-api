using System.ComponentModel;
using System.Globalization;

namespace ApiVenda.Application.Utils;

public static class Util
{
    public static string FormatarMoeda(double valor)
    {
        return valor.ToString("C", new System.Globalization.CultureInfo("pt-BR"));
    }

    public static string GerarDataHoraExataFormatoPtBr()
    {
        return DateTime.Now.ToString(CultureInfo.CreateSpecificCulture("pt-BR"));
    }

    public static string GetDescriptionEnum<T>(this T enumValue) where T : struct, IConvertible
    {
        if (!typeof(T).IsEnum)
            return null;

        var description = enumValue.ToString();
        var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

        if (fieldInfo != null)
        {
            var attrs = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);

            if (attrs != null && attrs.Length > 0)
                description = ((DescriptionAttribute)attrs[0]).Description;
        }

        return description;
    }
}