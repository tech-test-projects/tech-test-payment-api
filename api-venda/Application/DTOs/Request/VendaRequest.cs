namespace ApiVenda.Application.DTO;

public class VendaRequest
{
    public int IdVendedor { get; set; }

    public List<ItemVendaRequest>? ItensVenda { get; set; }
}