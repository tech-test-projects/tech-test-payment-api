namespace ApiVenda.Application.DTO;

public class ItemVendaRequest
{
    public int IdProduto { get; set; }

    public int Quantidade { get; set; }
}